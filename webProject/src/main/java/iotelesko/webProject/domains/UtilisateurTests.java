package iotelesko.webProject.domains;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.time.ZonedDateTime;


/**
 * A UtilisateurTests.
 */
@Entity
@Table(name = "utilisateur_tests")

public class UtilisateurTests implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "test_score")
    private Integer testScore;

    @ManyToOne
    @JsonIgnoreProperties("utilisateurTests")
    @NotNull
    private Utilisateur utilisateur;

    @ManyToOne
    @JsonIgnoreProperties("utilisateurTests")
    @NotNull
    private Tests tests;
    
    

    /**
	 * @param createdDate
	 * @param testScore
	 * @param utilisateur
	 * @param tests
	 */
	public UtilisateurTests(ZonedDateTime createdDate, Integer testScore, @NotNull Utilisateur utilisateur,@NotNull Tests tests) {
		super();
		this.createdDate = createdDate;
		this.testScore = testScore;
		this.utilisateur = utilisateur;
		this.tests = tests;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public UtilisateurTests createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getTestScore() {
        return testScore;
    }

    public UtilisateurTests testScore(Integer testScore) {
        this.testScore = testScore;
        return this;
    }

    public void setTestScore(Integer testScore) {
        this.testScore = testScore;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public UtilisateurTests utilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
        return this;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Tests getTests() {
        return tests;
    }

    public UtilisateurTests tests(Tests tests) {
        this.tests = tests;
        return this;
    }

    public void setTests(Tests tests) {
        this.tests = tests;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UtilisateurTests)) {
            return false;
        }
        return id != null && id.equals(((UtilisateurTests) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UtilisateurTests{" +
            "id=" + getId() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", testScore=" + getTestScore() +
            "}";
    }
}