package iotelesko.webProject.domains;


import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Lesson.
 */
@Entity
@Table(name = "lesson")
public class Lesson implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "lesson_title", nullable = false)
    private String lessonTitle;

    @Column(name = "lesson_content")
    private String lessonContent;

    @Lob
    @Column(name = "pdf_content")
    private byte[] pdfContent;

    @Column(name = "pdf_content_content_type")
    private String pdfContentContentType;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "deleted_date")
    private ZonedDateTime deletedDate;

    @Column(name = "creation_date")
    private Instant creationDate;

    @ManyToOne
    @JsonIgnoreProperties("lessons")
    @NotNull
    private Chapter chapter;

    @OneToMany(mappedBy = "lesson")
    private Set<Tests> tests = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLessonTitle() {
        return lessonTitle;
    }

    public Lesson lessonTitle(String lessonTitle) {
        this.lessonTitle = lessonTitle;
        return this;
    }

    public void setLessonTitle(String lessonTitle) {
        this.lessonTitle = lessonTitle;
    }

    public String getLessonContent() {
        return lessonContent;
    }

    public Lesson lessonContent(String lessonContent) {
        this.lessonContent = lessonContent;
        return this;
    }

    public void setLessonContent(String lessonContent) {
        this.lessonContent = lessonContent;
    }

    public byte[] getPdfContent() {
        return pdfContent;
    }

    public Lesson pdfContent(byte[] pdfContent) {
        this.pdfContent = pdfContent;
        return this;
    }

    public void setPdfContent(byte[] pdfContent) {
        this.pdfContent = pdfContent;
    }

    public String getPdfContentContentType() {
        return pdfContentContentType;
    }

    public Lesson pdfContentContentType(String pdfContentContentType) {
        this.pdfContentContentType = pdfContentContentType;
        return this;
    }

    public void setPdfContentContentType(String pdfContentContentType) {
        this.pdfContentContentType = pdfContentContentType;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public Lesson createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Lesson deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getDeletedDate() {
        return deletedDate;
    }

    public Lesson deletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
        return this;
    }

    public void setDeletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public Lesson creationDate(Instant creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public Lesson chapter(Chapter chapter) {
        this.chapter = chapter;
        return this;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public Set<Tests> getTests() {
        return tests;
    }

    public Lesson tests(Set<Tests> tests) {
        this.tests = tests;
        return this;
    }

    public Lesson addTests(Tests tests) {
        this.tests.add(tests);
        tests.setLesson(this);
        return this;
    }

    public Lesson removeTests(Tests tests) {
        this.tests.remove(tests);
        tests.setLesson(null);
        return this;
    }

    public void setTests(Set<Tests> tests) {
        this.tests = tests;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Lesson lesson = (Lesson) o;
        if (lesson.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lesson.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Lesson{" +
            "id=" + getId() +
            ", lessonTitle='" + getLessonTitle() + "'" +
            ", lessonContent='" + getLessonContent() + "'" +
            ", pdfContent='" + getPdfContent() + "'" +
            ", pdfContentContentType='" + getPdfContentContentType() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", deletedDate='" + getDeletedDate() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            "}";
    }
}
