package iotelesko.webProject.domains;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Tests.
 */
@Entity
@Table(name = "tests")
public class Tests implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "deleted_date")
    private ZonedDateTime deletedDate;

    @NotNull
    @Column(name = "test_title", nullable = false)
    private String testTitle;

    @ManyToOne
    @JsonIgnoreProperties("tests")
    @NotNull
    private Lesson lesson;

    @OneToMany(mappedBy = "tests")
    private Set<Question> questions = new HashSet<>();

    @OneToMany(mappedBy = "tests")
    private Set<UtilisateurTests> utilisateurTests = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    
    
    
    public Tests(Long id) {
		super();
		this.id = id;
	}
    
    
    public Tests() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getId() {
        return id;
    }

	public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public Tests createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Tests deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getDeletedDate() {
        return deletedDate;
    }

    public Tests deletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
        return this;
    }

    public void setDeletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getTestTitle() {
        return testTitle;
    }

    public Tests testTitle(String testTitle) {
        this.testTitle = testTitle;
        return this;
    }

    public void setTestTitle(String testTitle) {
        this.testTitle = testTitle;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public Tests lesson(Lesson lesson) {
        this.lesson = lesson;
        return this;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public Tests questions(Set<Question> questions) {
        this.questions = questions;
        return this;
    }

    public Tests addQuestion(Question question) {
        this.questions.add(question);
        question.setTests(this);
        return this;
    }

    public Tests removeQuestion(Question question) {
        this.questions.remove(question);
        question.setTests(null);
        return this;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tests tests = (Tests) o;
        if (tests.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tests.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tests{" +
            "id=" + getId() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", deletedDate='" + getDeletedDate() + "'" +
            ", testTitle='" + getTestTitle() + "'" + getQuestions()+
            "}";
    }
}
