package iotelesko.webProject.domains.enumeration;

/**
 * The QuestionType enumeration.
 */
public enum QuestionType {
    MCQ, TRUE_FALSE
}
