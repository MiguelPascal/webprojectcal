package iotelesko.webProject.domains.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    STUDENT, ADMIN
}
