package iotelesko.webProject.domains;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import iotelesko.webProject.domains.enumeration.Role;

/**
 * A Utilisateur.
 */
@Entity
@Table(name = "utilisateur")
public class Utilisateur implements Serializable {

	 @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    @NotNull
	    @Column(name = "username", nullable = false)
	    private String username;

	    @NotNull
	    @Column(name = "jhi_password", nullable = false)
	    private String password;

	    @Column(name = "phone")
	    private Integer phone;

	    @Column(name = "first_name")
	    private String firstName;

	    @Column(name = "last_name")
	    private String lastName;

	    @Column(name = "sex")
	    private String sex;

	    @Column(name = "created_date")
	    private ZonedDateTime createdDate;

	    @Column(name = "deleted")
	    private Boolean deleted;

	    @Column(name = "deleted_date")
	    private ZonedDateTime deletedDate;

	    @NotNull
	    @Enumerated(EnumType.STRING)
	    @Column(name = "jhi_role", nullable = false)
	    private Role role;

	    @OneToMany(mappedBy = "utilisateur")
	    private Set<UtilisateurTests> utilisateurTests = new HashSet<>();

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public Utilisateur username(String username) {
		this.username = username;
		return this;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public Utilisateur password(String password) {
		this.password = password;
		return this;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getPhone() {
		return phone;
	}

	public Utilisateur phone(Integer phone) {
		this.phone = phone;
		return this;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}

	public Utilisateur createdDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
		return this;
	}

	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean isDeleted() {
		return deleted;
	}

	public Utilisateur deleted(Boolean deleted) {
		this.deleted = deleted;
		return this;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public ZonedDateTime getDeletedDate() {
		return deletedDate;
	}

	public Utilisateur deletedDate(ZonedDateTime deletedDate) {
		this.deletedDate = deletedDate;
		return this;
	}

	public void setDeletedDate(ZonedDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getRole() {
		return role.name();
	}

	public Utilisateur role(Role role) {
		this.role = role;
		return this;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	 public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public Set<UtilisateurTests> getUtilisateurTests() {
	        return utilisateurTests;
	    }

	    public Utilisateur utilisateurTests(Set<UtilisateurTests> utilisateurTests) {
	        this.utilisateurTests = utilisateurTests;
	        return this;
	    }

	    public Utilisateur addUtilisateurTests(UtilisateurTests utilisateurTests) {
	        this.utilisateurTests.add(utilisateurTests);
	        utilisateurTests.setUtilisateur(this);
	        return this;
	    }

	    public Utilisateur removeUtilisateurTests(UtilisateurTests utilisateurTests) {
	        this.utilisateurTests.remove(utilisateurTests);
	        utilisateurTests.setUtilisateur(null);
	        return this;
	    }

	    public void setUtilisateurTests(Set<UtilisateurTests> utilisateurTests) {
	        this.utilisateurTests = utilisateurTests;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	    @Override
	    public boolean equals(Object o) {
	        if (this == o) {
	            return true;
	        }
	        if (!(o instanceof Utilisateur)) {
	            return false;
	        }
	        return id != null && id.equals(((Utilisateur) o).id);
	    }

	    @Override
	    public int hashCode() {
	        return 31;
	    }

	    @Override
	    public String toString() {
	        return "Utilisateur{" +
	            "id=" + getId() +
	            ", username='" + getUsername() + "'" +
	            ", password='" + getPassword() + "'" +
	            ", phone=" + getPhone() +
	            ", firstName='" + getFirstName() + "'" +
	            ", lastName='" + getLastName() + "'" +
	            ", sex='" + getSex() + "'" +
	            ", createdDate='" + getCreatedDate() + "'" +
	            ", deleted='" + isDeleted() + "'" +
	            ", deletedDate='" + getDeletedDate() + "'" +
	            ", role='" + getRole() + "'" +
	            "}";
	    }

}
