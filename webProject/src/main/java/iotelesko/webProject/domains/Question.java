package iotelesko.webProject.domains;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import iotelesko.webProject.domains.enumeration.QuestionType;

/**
 * A Question.
 */
@Entity
@Table(name = "question")
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "deleted_date")
    private ZonedDateTime deletedDate;

    @Column(name = "right_answer")
    private String rightAnswer;

    @Column(name = "question_title")
    private String questionTitle;

    @Column(name = "question_score")
    private Integer questionScore;

    @Enumerated(EnumType.STRING)
    @Column(name = "question_type")
    private QuestionType questionType;

    @ManyToOne
    @JsonIgnoreProperties("questions")
    
    private Tests tests;

    @OneToMany(mappedBy = "question")
    private Set<WrongAnswer> wrongAnswers = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    
    public Question() {
		super();
		// TODO Auto-generated constructor stub
	}

    
    public Question(Long id) {
		super();
		this.id = id;
	}
    
   	public Question(ZonedDateTime createdDate, String rightAnswer, String questionTitle, Integer questionScore,
			QuestionType questionType) {
		super();
		this.createdDate = createdDate;
		this.rightAnswer = rightAnswer;
		this.questionTitle = questionTitle;
		this.questionScore = questionScore;
		this.questionType = questionType;
	}



	public Question(@NotNull Tests tests) {
		super();
		this.tests = tests;
	}



	public Question(Tests test,String questionTitle,Integer questionScore,QuestionType questionType,String rightAnswer) {
		super();
		this.createdDate = ZonedDateTime.now();
		this.deleted = false;
		this.rightAnswer = rightAnswer;
		this.questionTitle = questionTitle;
		this.questionScore = questionScore;
		this.questionType = questionType;
		this.tests=test;
	}
	public Question(ZonedDateTime createdDate, String rightAnswer, String questionTitle,
			Integer questionScore) {
		super();
		this.createdDate = createdDate;
		this.deleted = false;
		this.rightAnswer = rightAnswer;
		this.questionTitle = questionTitle;
		this.questionScore = questionScore;
	}
	public Long getId() {
        return id;
    }

	public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public Question createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Question deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getDeletedDate() {
        return deletedDate;
    }

    public Question deletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
        return this;
    }

    public void setDeletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getRightAnswer() {
        return rightAnswer;
    }

    public Question rightAnswer(String rightAnswer) {
        this.rightAnswer = rightAnswer;
        return this;
    }

    public void setRightAnswer(String rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public Question questionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
        return this;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public Integer getQuestionScore() {
        return questionScore;
    }

    public Question questionScore(Integer questionScore) {
        this.questionScore = questionScore;
        return this;
    }

    public void setQuestionScore(Integer questionScore) {
        this.questionScore = questionScore;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public Question questionType(QuestionType questionType) {
        this.questionType = questionType;
        return this;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public Tests getTests() {
        return tests;
    }

    public Question tests(Tests tests) {
        this.tests = tests;
        return this;
    }

    public void setTests(Tests tests) {
        this.tests = tests;
    }

    public Set<WrongAnswer> getWrongAnswers() {
        return wrongAnswers;
    }

    public Question wrongAnswers(Set<WrongAnswer> wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
        return this;
    }

    public Question addWrongAnswer(WrongAnswer wrongAnswer) {
        this.wrongAnswers.add(wrongAnswer);
        wrongAnswer.setQuestion(this);
        return this;
    }

    public Question removeWrongAnswer(WrongAnswer wrongAnswer) {
        this.wrongAnswers.remove(wrongAnswer);
        wrongAnswer.setQuestion(null);
        return this;
    }

    public void setWrongAnswers(Set<WrongAnswer> wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Question question = (Question) o;
        if (question.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), question.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Question{" +
            "id=" + getId() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", deletedDate='" + getDeletedDate() + "'" +
            ", rightAnswer='" + getRightAnswer() + "'" +
            ", questionTitle='" + getQuestionTitle() + "'" +
            ", questionScore=" + getQuestionScore() +
            ", questionType='" + getQuestionType() + "'" +
            "}";
    }
}
