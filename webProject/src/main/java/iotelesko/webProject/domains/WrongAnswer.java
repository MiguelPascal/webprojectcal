package iotelesko.webProject.domains;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A WrongAnswer.
 */
@Entity
@Table(name = "wrong_answer")
public class WrongAnswer implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "deleted_date")
    private ZonedDateTime deletedDate;

    @Column(name = "wrong_answer")
    private String wrongAnswer;

    @ManyToOne
    @JsonIgnoreProperties("wrongAnswers")
    @NotNull
    private Question question;
    
    

    public WrongAnswer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WrongAnswer(String wrongAnswer, @NotNull Question question) {
		super();
		this.createdDate=ZonedDateTime.now();
		this.deleted=false;
		this.wrongAnswer = wrongAnswer;
		this.question = question;
	}



	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public WrongAnswer createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public WrongAnswer deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getDeletedDate() {
        return deletedDate;
    }

    public WrongAnswer deletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
        return this;
    }

    public void setDeletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getWrongAnswer() {
        return wrongAnswer;
    }

    public WrongAnswer wrongAnswer(String wrongAnswer) {
        this.wrongAnswer = wrongAnswer;
        return this;
    }

    public void setWrongAnswer(String wrongAnswer) {
        this.wrongAnswer = wrongAnswer;
    }

    public Question getQuestion() {
        return question;
    }

    public WrongAnswer question(Question question) {
        this.question = question;
        return this;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WrongAnswer wrongAnswer = (WrongAnswer) o;
        if (wrongAnswer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wrongAnswer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WrongAnswer{" +
            "id=" + getId() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", deletedDate='" + getDeletedDate() + "'" +
            ", wrongAnswer='" + getWrongAnswer() + "'" +
            "}";
    }
}
