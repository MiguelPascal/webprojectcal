package iotelesko.webProject.domains;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Module.
 */
@Entity
@Table(name = "module")
public class Module implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "module_title", nullable = false)
    private String moduleTitle;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "deleted_date")
    private ZonedDateTime deletedDate;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "module")
    private Set<Chapter> chapters = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    
    public Module() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    
    public Module(Long id) {
		super();
		this.id = id;
	}


	public Module(@NotNull String moduleTitle, ZonedDateTime createdDate, String description) {
		super();
		this.moduleTitle = moduleTitle;
		this.createdDate = createdDate;
		this.description = description;
		this.deleted = false;
		
	}

	public Module(Long id, @NotNull String moduleTitle,String description) {
		super();
		this.id = id;
		this.moduleTitle = moduleTitle;
		this.description = description;
		this.createdDate = ZonedDateTime.now();
		this.deleted = false;
	}

	public Long getId() {
        return id;
    }

	public void setId(Long id) {
        this.id = id;
    }

    public String getModuleTitle() {
        return moduleTitle;
    }

    public Module moduleTitle(String moduleTitle) {
        this.moduleTitle = moduleTitle;
        return this;
    }

    public void setModuleTitle(String moduleTitle) {
        this.moduleTitle = moduleTitle;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public Module createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Module deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getDeletedDate() {
        return deletedDate;
    }

    public Module deletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
        return this;
    }

    public void setDeletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDescription() {
        return description;
    }

    public Module description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Chapter> getChapters() {
        return chapters;
    }

    public Module chapters(Set<Chapter> chapters) {
        this.chapters = chapters;
        return this;
    }

    public Module addChapter(Chapter chapter) {
        this.chapters.add(chapter);
        chapter.setModule(this);
        return this;
    }

    public Module removeChapter(Chapter chapter) {
        this.chapters.remove(chapter);
        chapter.setModule(null);
        return this;
    }

    public void setChapters(Set<Chapter> chapters) {
        this.chapters = chapters;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Module module = (Module) o;
        if (module.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), module.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Module{" +
            "id=" + getId() +
            ", moduleTitle='" + getModuleTitle() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", deletedDate='" + getDeletedDate() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
