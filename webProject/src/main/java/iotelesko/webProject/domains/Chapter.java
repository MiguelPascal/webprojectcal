package iotelesko.webProject.domains;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Chapter.
 */
@Entity
@Table(name = "chapter")
public class Chapter implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "chapter_title", nullable = false)
    private String chapterTitle;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "deleted_date")
    private ZonedDateTime deletedDate;

    @Column(name = "chapter_description")
    private String chapterDescription;

    @ManyToOne
    @JsonIgnoreProperties("chapters")
    @NotNull
    private Module module;

    @OneToMany(mappedBy = "chapter")
    private Set<Lesson> lessons = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    
    

    public Chapter() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    
    public Chapter(Long id) {
		super();
		this.id = id;
	}


	public Chapter(@NotNull String chapterTitle, ZonedDateTime createdDate, String chapterDescription,@NotNull Module module) {
		super();
		this.chapterTitle = chapterTitle;
		this.createdDate = createdDate;
		this.chapterDescription = chapterDescription;
		this.module=module;
		this.deleted=false;
	}

    
	public Chapter(@NotNull Long id, @NotNull String chapterTitle, String chapterDescription,@NotNull Module module) {
		super();
		this.id = id;
		this.chapterTitle = chapterTitle;
		this.chapterDescription = chapterDescription;
		this.createdDate = ZonedDateTime.now();
		this.module=module;
		this.deleted=false;
	}

	public Long getId() {
        return id;
    }

	public void setId(Long id) {
        this.id = id;
    }

    public String getChapterTitle() {
        return chapterTitle;
    }

    public Chapter chapterTitle(String chapterTitle) {
        this.chapterTitle = chapterTitle;
        return this;
    }

    public void setChapterTitle(String chapterTitle) {
        this.chapterTitle = chapterTitle;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public Chapter createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Chapter deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getDeletedDate() {
        return deletedDate;
    }

    public Chapter deletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
        return this;
    }

    public void setDeletedDate(ZonedDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getChapterDescription() {
        return chapterDescription;
    }

    public Chapter chapterDescription(String chapterDescription) {
        this.chapterDescription = chapterDescription;
        return this;
    }

    public void setChapterDescription(String chapterDescription) {
        this.chapterDescription = chapterDescription;
    }

    public Module getModule() {
        return module;
    }

    public Chapter module(Module module) {
        this.module = module;
        return this;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public Set<Lesson> getLessons() {
        return lessons;
    }

    public Chapter lessons(Set<Lesson> lessons) {
        this.lessons = lessons;
        return this;
    }

    public Chapter addLesson(Lesson lesson) {
        this.lessons.add(lesson);
        lesson.setChapter(this);
        return this;
    }

    public Chapter removeLesson(Lesson lesson) {
        this.lessons.remove(lesson);
        lesson.setChapter(null);
        return this;
    }

    public void setLessons(Set<Lesson> lessons) {
        this.lessons = lessons;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Chapter chapter = (Chapter) o;
        if (chapter.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chapter.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Chapter{" +
            "id=" + getId() +
            ", chapterTitle='" + getChapterTitle() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", deleted='" + isDeleted() + "'" +
            ", deletedDate='" + getDeletedDate() + "'" +
            ", chapterDescription='" + getChapterDescription() + "'" +
            "}";
    }
}
