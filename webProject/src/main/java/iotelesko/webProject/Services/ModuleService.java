package iotelesko.webProject.Services;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import iotelesko.webProject.domains.Module;

import java.util.Optional;

/**
 * Service Interface for managing Module.
 */
public interface ModuleService {

    /**
     * Save a module.
     *
     * @param module the entity to save
     * @return the persisted entity
     */
    Module save(Module module);

    /**
     * Get all the modules.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Module> findAll(Pageable pageable);


    /**
     * Get the "id" module.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Module> findOne(Long id);

    /**
     * Delete the "id" module.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the module corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Module> search(String query, Pageable pageable);
}
