package iotelesko.webProject.Services;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import iotelesko.webProject.domains.Chapter;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Chapter.
 */
public interface ChapterService {

    /**
     * Save a chapter.
     *
     * @param chapter the entity to save
     * @return the persisted entity
     */
    Chapter save(Chapter chapter);

    /**
     * Get all the chapters.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Chapter> findAll(Pageable pageable);


    /**
     * Get the "id" chapter.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Chapter> findOne(Long id);

    /**
     * Delete the "id" chapter.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the chapter corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Chapter> search(String query, Pageable pageable);
    
    Page <Chapter> findByModule(Long module,Pageable pageable);
}
