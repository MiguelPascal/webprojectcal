package iotelesko.webProject.Services;

import iotelesko.webProject.domains.UtilisateurTests;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link UtilisateurTests}.
 */
public interface UtilisateurTestsService {

    /**
     * Save a utilisateurTests.
     *
     * @param utilisateurTests the entity to save.
     * @return the persisted entity.
     */
    UtilisateurTests save(UtilisateurTests utilisateurTests);

    /**
     * Get all the utilisateurTests.
     *
     * @return the list of entities.
     */
    List<UtilisateurTests> findAll();


    /**
     * Get the "id" utilisateurTests.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UtilisateurTests> findOne(Long id);

    /**
     * Delete the "id" utilisateurTests.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
