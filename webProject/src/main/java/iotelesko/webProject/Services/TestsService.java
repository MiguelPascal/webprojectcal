package iotelesko.webProject.Services;


import java.util.List;
import java.util.Optional;

import iotelesko.webProject.domains.Tests;

/**
 * Service Interface for managing Tests.
 */
public interface TestsService {

    /**
     * Save a tests.
     *
     * @param tests the entity to save
     * @return the persisted entity
     */
    Tests save(Tests tests);

    /**
     * Get all the tests.
     *
     * @return the list of entities
     */
    List<Tests> findAll();


    /**
     * Get the "id" tests.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Tests> findOne(Long id);

    /**
     * Delete the "id" tests.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the tests corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @return the list of entities
     */
    List<Tests> search(String query);
}
