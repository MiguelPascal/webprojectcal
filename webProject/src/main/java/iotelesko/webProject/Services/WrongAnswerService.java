package iotelesko.webProject.Services;


import java.util.List;
import java.util.Optional;

import iotelesko.webProject.domains.WrongAnswer;

/**
 * Service Interface for managing WrongAnswer.
 */
public interface WrongAnswerService {

    /**
     * Save a wrongAnswer.
     *
     * @param wrongAnswer the entity to save
     * @return the persisted entity
     */
    WrongAnswer save(WrongAnswer wrongAnswer);

    /**
     * Get all the wrongAnswers.
     *
     * @return the list of entities
     */
    List<WrongAnswer> findAll();


    /**
     * Get the "id" wrongAnswer.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WrongAnswer> findOne(Long id);

    /**
     * Delete the "id" wrongAnswer.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wrongAnswer corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @return the list of entities
     */
    List<WrongAnswer> search(String query);
}
