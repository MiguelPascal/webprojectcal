package iotelesko.webProject.Services;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import iotelesko.webProject.domains.Utilisateur;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Utilisateur.
 */
public interface UtilisateurService {

    /**
     * Save a utilisateur.
     *
     * @param utilisateur the entity to save.
     * @return the persisted entity.
     */
    Utilisateur save(Utilisateur utilisateur);

    /**
     * Get all the utilisateurs.
     *
     * @return the list of entities.
     */
    List<Utilisateur> findAll();


    /**
     * Get the "id" utilisateur.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Utilisateur> findOne(Long id);

    /**
     * Delete the "id" utilisateur.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    /**
     * Search for the utilisateur corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @return the list of entities
     */
    List<Utilisateur> search(String query);

	Utilisateur findByUsername(String username);

}
