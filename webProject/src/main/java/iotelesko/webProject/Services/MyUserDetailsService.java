//package iotelesko.webProject.Services;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import iotelesko.webProject.Models.User;
//import iotelesko.webProject.Repository.UserRepository;
//
//@Service
//public class MyUserDetailsService implements UserDetailsService {
//
//	@Autowired
//	private UserRepository userRepo;
//	
//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		
//		User user=userRepo.findByUsername(username);
//		
//		if(user==null) {
//			throw new UsernameNotFoundException("User 404") ; 
//		} 
//				
//		return new UserPrincipal(user);
//	}
//
//}
