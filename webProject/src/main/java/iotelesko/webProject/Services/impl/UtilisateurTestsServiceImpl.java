package iotelesko.webProject.Services.impl;

import iotelesko.webProject.Services.UtilisateurTestsService;
import iotelesko.webProject.domains.UtilisateurTests;
import iotelesko.webProject.Repository.UtilisateurTestsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link UtilisateurTests}.
 */
@Service
@Transactional
public class UtilisateurTestsServiceImpl implements UtilisateurTestsService {

    private final Logger log = LoggerFactory.getLogger(UtilisateurTestsServiceImpl.class);

    private final UtilisateurTestsRepository utilisateurTestsRepository;

    public UtilisateurTestsServiceImpl(UtilisateurTestsRepository utilisateurTestsRepository) {
        this.utilisateurTestsRepository = utilisateurTestsRepository;
    }

    /**
     * Save a utilisateurTests.
     *
     * @param utilisateurTests the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UtilisateurTests save(UtilisateurTests utilisateurTests) {
        log.debug("Request to save UtilisateurTests : {}", utilisateurTests);
        return utilisateurTestsRepository.save(utilisateurTests);
    }

    /**
     * Get all the utilisateurTests.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<UtilisateurTests> findAll() {
        log.debug("Request to get all UtilisateurTests");
        return utilisateurTestsRepository.findAll();
    }


    /**
     * Get one utilisateurTests by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UtilisateurTests> findOne(Long id) {
        log.debug("Request to get UtilisateurTests : {}", id);
        return utilisateurTestsRepository.findById(id);
    }

    /**
     * Delete the utilisateurTests by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UtilisateurTests : {}", id);
        utilisateurTestsRepository.deleteById(id);
    }
}
