package iotelesko.webProject.Services.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import iotelesko.webProject.Repository.WrongAnswerRepository;
import iotelesko.webProject.Services.WrongAnswerService;
import iotelesko.webProject.domains.WrongAnswer;

/**
 * Service Implementation for managing WrongAnswer.
 */
@Service
@Transactional
public class WrongAnswerServiceImpl implements WrongAnswerService {

    private final Logger log = LoggerFactory.getLogger(WrongAnswerServiceImpl.class);

    private final WrongAnswerRepository wrongAnswerRepository;


    public WrongAnswerServiceImpl(WrongAnswerRepository wrongAnswerRepository) {
        this.wrongAnswerRepository = wrongAnswerRepository;
    }

    /**
     * Save a wrongAnswer.
     *
     * @param wrongAnswer the entity to save
     * @return the persisted entity
     */
    @Override
    public WrongAnswer save(WrongAnswer wrongAnswer) {
        log.debug("Request to save WrongAnswer : {}", wrongAnswer);
        WrongAnswer result = wrongAnswerRepository.save(wrongAnswer);
       // wrongAnswerSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the wrongAnswers.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<WrongAnswer> findAll() {
        log.debug("Request to get all WrongAnswers");
        return wrongAnswerRepository.findAll();
    }


    /**
     * Get one wrongAnswer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WrongAnswer> findOne(Long id) {
        log.debug("Request to get WrongAnswer : {}", id);
        return wrongAnswerRepository.findById(id);
    }

    /**
     * Delete the wrongAnswer by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WrongAnswer : {}", id);
        wrongAnswerRepository.deleteById(id);
       // wrongAnswerSearchRepository.deleteById(id);
    }

    /**
     * Search for the wrongAnswer corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<WrongAnswer> search(String query) {
        log.debug("Request to search WrongAnswers for query {}", query);
        /*
		 * StreamSupport
		 * .stream(wrongAnswerSearchRepository.search(queryStringQuery(query)).
		 * spliterator(), false) .collect(Collectors.toList())
		 */
		return null;
    }
}
