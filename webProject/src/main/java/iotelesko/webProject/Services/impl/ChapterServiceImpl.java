package iotelesko.webProject.Services.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import iotelesko.webProject.Repository.ChapterRepository;
import iotelesko.webProject.Services.ChapterService;
import iotelesko.webProject.domains.Chapter;

/**
 * Service Implementation for managing Chapter.
 */
@Service
@Transactional
public class ChapterServiceImpl implements ChapterService {

    private final Logger log = LoggerFactory.getLogger(ChapterServiceImpl.class);

    private final ChapterRepository chapterRepository;

   

    public ChapterServiceImpl(ChapterRepository chapterRepository) {
        this.chapterRepository = chapterRepository;
       
    }

    /**
     * Save a chapter.
     *
     * @param chapter the entity to save
     * @return the persisted entity
     */
    @Override
    public Chapter save(Chapter chapter) {
        log.debug("Request to save Chapter : {}", chapter);
        Chapter result = chapterRepository.save(chapter);
        return result;
    }

    /**
     * Get all the chapters.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Chapter> findAll(Pageable pageable) {
        log.debug("Request to get all Chapters");
        return chapterRepository.findAll(pageable);
    }
    /**
     * Get all the chapters according to a module.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page <Chapter> findByModule(Long module,Pageable pageable) {
        log.debug("Request to get all Chapters");
        return chapterRepository.findByModule(module,pageable);
    }

    /**
     * Get one chapter by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Chapter> findOne(Long id) {
        log.debug("Request to get Chapter : {}", id);
        return chapterRepository.findById(id);
    }

    /**
     * Delete the chapter by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Chapter : {}", id);        
        chapterRepository.deleteById(id);
    }

    /**
     * Search for the chapter corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Chapter> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Chapters for query {}", query);
        return null;
      }
}
