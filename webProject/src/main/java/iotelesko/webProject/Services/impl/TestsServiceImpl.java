package iotelesko.webProject.Services.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import iotelesko.webProject.Repository.TestsRepository;
import iotelesko.webProject.Services.TestsService;
import iotelesko.webProject.domains.Tests;

/**
 * Service Implementation for managing Tests.
 */
@Service
@Transactional
public class TestsServiceImpl implements TestsService {

    private final Logger log = LoggerFactory.getLogger(TestsServiceImpl.class);

    private final TestsRepository testsRepository;


    public TestsServiceImpl(TestsRepository testsRepository) {
        this.testsRepository = testsRepository;
    }

    /**
     * Save a tests.
     *
     * @param tests the entity to save
     * @return the persisted entity
     */
    @Override
    public Tests save(Tests tests) {
        log.debug("Request to save Tests : {}", tests);
        Tests result = testsRepository.save(tests);
        return result;
    }

    /**
     * Get all the tests.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Tests> findAll() {
        log.debug("Request to get all Tests");
        return testsRepository.findAll();
    }


    /**
     * Get one tests by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Tests> findOne(Long id) {
        log.debug("Request to get Tests : {}", id);
        return testsRepository.findById(id);
    }

    /**
     * Delete the tests by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Tests : {}", id);        testsRepository.deleteById(id);
    }

    /**
     * Search for the tests corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Tests> search(String query) {
        log.debug("Request to search Tests for query {}", query);
        return null;
    }
}
