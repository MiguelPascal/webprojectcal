package iotelesko.webProject.Controller;

import java.time.ZonedDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import iotelesko.webProject.Services.ChapterService;
import iotelesko.webProject.Services.ModuleService;
import iotelesko.webProject.Services.QuestionService;
import iotelesko.webProject.Services.WrongAnswerService;
import iotelesko.webProject.domains.Chapter;
import iotelesko.webProject.domains.Lesson;
import iotelesko.webProject.domains.Module;
import iotelesko.webProject.domains.Question;
import iotelesko.webProject.domains.Tests;
import iotelesko.webProject.domains.WrongAnswer;
import iotelesko.webProject.domains.enumeration.QuestionType;
import iotelesko.webProject.utils.ChapterForm;
import iotelesko.webProject.utils.ModuleForm;
import iotelesko.webProject.utils.QuestionForm;
import iotelesko.webProject.utils.Utilisateur;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	ModuleService moduleService;
	@Autowired
	ChapterService chapterService;
	@Autowired
	QuestionService questionService;
	@Autowired
	WrongAnswerService wrongAnswerService;

	@SuppressWarnings("unchecked")
	@GetMapping("/contacts")
	public String contacts(Model model, HttpServletRequest request){
		String url = "http://localhost:8080/api/utilisateurs";
		List<Utilisateur> users = restTemplate.getForObject(url, List.class);
		
		model.addAttribute("users", users);
		
		return "/admin/contacts";
	}
	@GetMapping("/chaptersView")
	public String viewChapters(Model model) {
			String url = "http://localhost:8080/api/chapters";
			@SuppressWarnings("unchecked")
			List<Chapter> chapters = restTemplate.getForObject(url, List.class);
			String url2= "http://localhost:8080/api/modules";
			@SuppressWarnings("unchecked")
			List<Module> modules = restTemplate.getForObject(url2, List.class);
			model.addAttribute("modules", modules);
			model.addAttribute("chapters", chapters);
			model.addAttribute("chapterForm", new ChapterForm());
		return "admin/chaptersAdmin";
	}

	@GetMapping("/modulesView")
	public String viewModules(Model model) {
			String url = "http://localhost:8080/api/modules";
			@SuppressWarnings("unchecked")
			List<Module> modules = restTemplate.getForObject(url, List.class);
			model.addAttribute("moduleForm", new ModuleForm());
			model.addAttribute("modules", modules);
		return "admin/modulesAdmin";
	}
	
	@GetMapping("/lessonsView")
	public String viewLessons(Model model) {
		String url = "http://localhost:8080/api/lessons";
		@SuppressWarnings("unchecked")
		List<Lesson> lessons = restTemplate.getForObject(url, List.class);
		model.addAttribute("lessons", lessons);
		return "admin/lessonsAdmin";
	}
	@GetMapping("/testsView")
	public String viewTests(Model model) {
		String url = "http://localhost:8080/api/lessons";
		@SuppressWarnings("unchecked")
		List<Lesson> lessons = restTemplate.getForObject(url, List.class);
		
		String url2 = "http://localhost:8080/api/tests";
		@SuppressWarnings("unchecked")
		List<Tests> tests = restTemplate.getForObject(url2, List.class);
		model.addAttribute("testForm", new iotelesko.webProject.utils.Tests());
		model.addAttribute("tests", tests);
		model.addAttribute("lessons", lessons);
		return "admin/testsAdmin";
	}
	
	@GetMapping("/questionsView")
	public String viewQuestions(Model model) {
		String url = "http://localhost:8080/api/questions";
		@SuppressWarnings("unchecked")
		List<Question> questions = restTemplate.getForObject(url, List.class);
		String url2 = "http://localhost:8080/api/tests";
		@SuppressWarnings("unchecked")
		List<Tests> tests = restTemplate.getForObject(url2, List.class);
		model.addAttribute("questionForm", new QuestionForm());
		model.addAttribute("tests", tests);
		model.addAttribute("questions", questions);
		return "admin/questionsAdmin";
	}
	
	
	@PostMapping("/saveModule")
	public String createModule(Model model,@Valid ModuleForm module,BindingResult bindingResult) {
		
		if (bindingResult.hasErrors()) {
			System.out.println("BINDING RESULT ERROR");
			return "redirect:/admin/modulesView";
		}
		moduleService.save(new Module(module.getModuleTitle(),ZonedDateTime.now(),module.getModuleDescription()));
		
		return "redirect:/admin/modulesView";
	}
	
	
	@PostMapping("/editModule")
	public String editModule(Model model,@Valid ModuleForm module,BindingResult bindingResult) {
		
		if (bindingResult.hasErrors()) {
			System.out.println("BINDING RESULT ERROR");
			return "redirect:/admin/modulesView";
		}
		moduleService.save(new Module(module.getModuleId(),module.getModuleTitle(),module.getModuleDescription()));
		
		return "redirect:/admin/modulesView";
	}
	
	@PostMapping("/saveChapter")
	public String createChapter(Model model,@Valid ChapterForm chapter,BindingResult bindingResult) {
		
		if (bindingResult.hasErrors()) {
			System.out.println("BINDING RESULT ERROR");
			return "redirect:/admin/chaptersView";
		}
		chapterService.save(new Chapter(chapter.getChapterTitle(),ZonedDateTime.now(),chapter.getChapterDescription(),new Module(chapter.getIdModule())));
		
		return "redirect:/admin/chaptersView";
	}
	
	@PostMapping("/editChapter")
	public String editChapter(Model model,@Valid ChapterForm chapter,BindingResult bindingResult) {
		
		if (bindingResult.hasErrors()) {
			System.out.println("BINDING RESULT ERROR");
			return "redirect:/admin/chaptersView";
		}
		chapterService.save(new Chapter(chapter.getChapterId(),chapter.getChapterTitle(),chapter.getChapterDescription(),new Module(chapter.getIdModule())));
		
		return "redirect:/admin/chaptersView";
	}
	
	@PostMapping("/saveQuestion")
	public String createQuestion(Model model,@Valid QuestionForm question,BindingResult bindingResult) {
		
		if (bindingResult.hasErrors()) {
			
			System.out.println("BINDING RESULT ERROR");
			
			return "redirect:/admin/questionsView";
			
		}
		System.out.println(question);
		questionService.save(new Question(new Tests(question.getIdTest()),question.getQuestionTitle(),question.getQuestionScore(),QuestionType.valueOf(question.getQuestionType()),question.getRightAnswer()));
		
		@SuppressWarnings("unchecked")
		List<Question> questions = restTemplate.getForObject("http://localhost:8080/api/questions", List.class);
		
		if((question.getQuestionType()).equals("MCQ")) {
			wrongAnswerService.save(new WrongAnswer(question.getWrongAnswer1(), new Question((long) questions.size())));
			wrongAnswerService.save(new WrongAnswer(question.getWrongAnswer2(), new Question((long) questions.size())));
			wrongAnswerService.save(new WrongAnswer(question.getWrongAnswer3(), new Question((long) questions.size())));
		}
		wrongAnswerService.save(new WrongAnswer(question.getWrongAnswer1(), new Question((long) questions.size())));
		
		return "redirect:/admin/questionsView";
	}

}
