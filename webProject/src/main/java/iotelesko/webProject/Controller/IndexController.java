package iotelesko.webProject.Controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import iotelesko.webProject.Services.UtilisateurService;
import iotelesko.webProject.domains.Lesson;
import iotelesko.webProject.domains.Question;
import iotelesko.webProject.domains.Utilisateur;
import iotelesko.webProject.utils.LoginForm;

@Controller
public class IndexController {
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	UtilisateurService utilisateurService;
	
	@RequestMapping("/")
	public String Login(Model model) {
		String message = "";
		model.addAttribute("message", message);
		model.addAttribute("userForm", new LoginForm());
		return "login";
	}

	@SuppressWarnings({ "unused", "unlikely-arg-type" })
	@PostMapping("/login")
	public String Home(@Valid LoginForm userForm, BindingResult bindingResult, Model model, HttpSession session) {
		if (bindingResult.hasErrors()) {
			System.out.println("BINDING RESULT ERROR");
			return "redirect:/";
		}
		Utilisateur user = utilisateurService.findByUsername(userForm.getUsername());
		
		if (user != null && user.getPassword().equals(userForm.getPassword())) {

			session.setAttribute("role", user.getRole());
			session.setAttribute("username", user.getUsername());
			session.setAttribute("idUser", user.getId());
			model.addAttribute("user", user);
			
			if (user.getRole().equals("STUDENT")) {
				return "redirect:/index";
			} else
				return "redirect:/dashboard";

		}
		String message = "Unknown username ou Wrong password !!";
		model.addAttribute("userForm", userForm);
		model.addAttribute("message", message);
		return "login";
	}

	@GetMapping("/logout")
	public String logout(HttpServletRequest request) {
		 /* Récupération et destruction de la session en cours */
        HttpSession session = request.getSession();
        session.invalidate();
        return "redirect:/";
	}

	@GetMapping("/index")
	public String home() {
		return "index";
	}
	
	@GetMapping("/dashboard")
	public String dashboard(Model model) {
		List<Question> questions = restTemplate.getForObject("http://localhost:8080/api/questions", List.class);
		List<Lesson> lessons = restTemplate.getForObject("http://localhost:8080/api/lessons", List.class);
		List<Utilisateur> users = restTemplate.getForObject("http://localhost:8080/api/utilisateurs", List.class);
		model.addAttribute("nbQuestions",questions.size());
		model.addAttribute("nbLessons", lessons.size());
		model.addAttribute("nbUsers", users.size());
		return "dashboard";
	}
	
}
