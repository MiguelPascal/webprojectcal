package iotelesko.webProject.Controller;



import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import iotelesko.webProject.Services.UtilisateurTestsService;
import iotelesko.webProject.domains.Chapter;
import iotelesko.webProject.domains.Lesson;
import iotelesko.webProject.domains.Module;
import iotelesko.webProject.domains.Question;
import iotelesko.webProject.domains.Tests;
import iotelesko.webProject.domains.Utilisateur;
import iotelesko.webProject.domains.UtilisateurTests;
//import iotelesko.webProject.utils.Utilisateur;

@Controller
@RequestMapping("/student")
public class StudentController {
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	UtilisateurTestsService utilisateurTestService;
	
	@GetMapping("/viewModules")
	public String viewModules(Model model) {
		String url = "http://localhost:8080/api/modules";
		@SuppressWarnings("unchecked")
		List<Module> modules = restTemplate.getForObject(url, List.class);
		model.addAttribute("modules", modules);
		return "/student/modulesView";
	}
	

	@GetMapping("/viewChapters")
	public String viewChapters(Model model) {
		String url = "http://localhost:8080/api/chapters";
		@SuppressWarnings("unchecked")
		List<Chapter> chapters = restTemplate.getForObject(url, List.class);
		model.addAttribute("chapters", chapters);
		return "/student/chaptersView";
	}
	
	@GetMapping("/viewChaptersModule/{id}")
	public String viewChaptersModule(Model model, @PathVariable("id") Long id) {
		String url = "http://localhost:8080/api/modules/"+ id;
		Module module = restTemplate.getForObject(url, Module.class);
		model.addAttribute("chapters",module.getChapters());
		model.addAttribute("idModule", id);
		return "/student/chaptersModuleView";
	}
	@GetMapping("/viewChapter/{id}")
	public String viewChapter(Model model, @PathVariable("id") Long id) {
		
		String url = "http://localhost:8080/api/chapters/"+ id;
		Chapter chapter = restTemplate.getForObject(url, Chapter.class);
		model.addAttribute("chapterTitle", chapter.getChapterTitle());
		model.addAttribute("lessons",chapter.getLessons());
		model.addAttribute("idChapter", id);
		return "/student/chapterView";
	}

	
	@GetMapping("/viewLesson/{id}")
	public String viewLesson(Model model, @PathVariable("id") Long id) {
		String url = "http://localhost:8080/api/lessons/"+ id;
		Lesson lesson = restTemplate.getForObject(url, Lesson.class);
		model.addAttribute("lesson",lesson);
		return "/student/lessonView";
	}
	
	@GetMapping("/viewTestsLesson/{id}")
	public String viewTestsLesson(Model model, @PathVariable("id") Long id) {
		String url = "http://localhost:8080/api/lessons/"+ id;
		Lesson lesson = restTemplate.getForObject(url, Lesson.class);
		model.addAttribute("tests",lesson.getTests());
		model.addAttribute("idLesson", id);
		return "/student/lessonViewTest";
	}
	
	@GetMapping("/viewTest/{id}")
	public String viewTest(Model model, @PathVariable("id") Long id) {
		String url = "http://localhost:8080/api/tests/"+ id;
		@SuppressWarnings("unchecked")
		Optional<Tests> test = restTemplate.getForObject(url, Optional.class);
		model.addAttribute("test", test.get());
		model.addAttribute("idTest", id);
		return "/student/viewQuestionsTest";
	}
	
	@PostMapping("/submitTest")
	public String submtiTest(HttpServletRequest request,Model model) {
		Integer score=0;
		Integer totalScore=0;
		String grade;
		String testId= request.getParameter("testId");
		String lessonId= request.getParameter("lessonId");
		String userId= request.getParameter("userId");
		String testTitle= request.getParameter("testTitle");
		String[] questionsIds = request.getParameterValues("questionId");
		for (String questionId:questionsIds) {
			String url = "http://localhost:8080/api/questions/"+ questionId;
			@SuppressWarnings("unchecked")
			Question question= restTemplate.getForObject(url, Question.class);
			totalScore= totalScore+question.getQuestionScore();
			String correctAnswer=question.getRightAnswer();
			if(correctAnswer.equals(request.getParameter("question_"+questionId))) {
				
				score=score+question.getQuestionScore();
			}		
		}
//		String url = "http://localhost:8080/api/tests/"+ testId;
//		@SuppressWarnings("unchecked")
//		Tests test= restTemplate.getForObject(url, Tests.class);
//		String url1 = "http://localhost:8080/api/utilisateurs/"+userId ;
//		@SuppressWarnings("unchecked")
//		Utilisateur user= restTemplate.getForObject(url1,Utilisateur.class);
//		utilisateurTestService.save(new UtilisateurTests(ZonedDateTime.now(), score,user, test));
		model.addAttribute("testId", testId);
		model.addAttribute("lessonId", lessonId);
		model.addAttribute("testTitle", testTitle);
		model.addAttribute("totalScore", totalScore);
		model.addAttribute("score", score);
		if(score>(totalScore/2)-1) {
			grade="OUAOOUH CONGRATULAIONS!!!!!! ";
		} else grade= "OUPS!! YOU FAILED !!!!!! ";
		model.addAttribute("grade", grade);
		return "/student/testResult";
	}
	@GetMapping("/showAnswersTest/{id}")
	public String showAnswerTest(Model model, @PathVariable("id") Long id) {
		String url = "http://localhost:8080/api/tests/"+ id;
		@SuppressWarnings("unchecked")
		Optional<Tests> test = restTemplate.getForObject(url, Optional.class);
		model.addAttribute("test", test.get());
		model.addAttribute("idTest", id);
		return "/student/viewAnswerQuestionsTest";
	}
}
