package iotelesko.webProject.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import iotelesko.webProject.domains.Utilisateur;
import jdk.javadoc.internal.doclets.formats.html.markup.HtmlAttr.Role;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Utilisateur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
	
	Utilisateur findByUsername(String username);

}
