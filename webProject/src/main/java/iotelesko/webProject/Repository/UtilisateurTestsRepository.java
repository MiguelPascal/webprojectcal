package iotelesko.webProject.Repository;

import iotelesko.webProject.domains.UtilisateurTests;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UtilisateurTests entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UtilisateurTestsRepository extends JpaRepository<UtilisateurTests, Long> {

}
