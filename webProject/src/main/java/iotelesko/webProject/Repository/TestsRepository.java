package iotelesko.webProject.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import iotelesko.webProject.domains.Tests;


/**
 * Spring Data  repository for the Tests entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TestsRepository extends JpaRepository<Tests, Long> {

}
