package iotelesko.webProject.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import iotelesko.webProject.domains.WrongAnswer;


/**
 * Spring Data  repository for the WrongAnswer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WrongAnswerRepository extends JpaRepository<WrongAnswer, Long> {

}
