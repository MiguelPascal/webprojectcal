package iotelesko.webProject.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import iotelesko.webProject.domains.Module;


/**
 * Spring Data  repository for the Module entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModuleRepository extends JpaRepository<Module, Long> {

}
