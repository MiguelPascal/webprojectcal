package iotelesko.webProject.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import iotelesko.webProject.domains.Question;


/**
 * Spring Data  repository for the Question entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

}
