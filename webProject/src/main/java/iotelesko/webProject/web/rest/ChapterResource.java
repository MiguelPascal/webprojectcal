package iotelesko.webProject.web.rest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.tomcat.util.http.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import iotelesko.webProject.Services.ChapterService;
import iotelesko.webProject.domains.Chapter;
import iotelesko.webProject.web.rest.errors.BadRequestAlertException;
import iotelesko.webProject.web.rest.util.HeaderUtil;
import iotelesko.webProject.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Chapter.
 */
@RestController
@RequestMapping("/api")
public class ChapterResource {

    private final Logger log = LoggerFactory.getLogger(ChapterResource.class);

    private static final String ENTITY_NAME = "chapter";

    private final ChapterService chapterService;

    public ChapterResource(ChapterService chapterService) {
        this.chapterService = chapterService;
    }

    /**
     * POST  /chapters : Create a new chapter.
     *
     * @param chapter the chapter to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chapter, or with status 400 (Bad Request) if the chapter has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chapters")
    public ResponseEntity<Chapter> createChapter(@Valid @RequestBody Chapter chapter) throws URISyntaxException {
        log.debug("REST request to save Chapter : {}", chapter);
        if (chapter.getId() != null) {
            throw new BadRequestAlertException("A new chapter cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Chapter result = chapterService.save(chapter);
        return ResponseEntity.created(new URI("/api/chapters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chapters : Updates an existing chapter.
     *
     * @param chapter the chapter to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chapter,
     * or with status 400 (Bad Request) if the chapter is not valid,
     * or with status 500 (Internal Server Error) if the chapter couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chapters")
    public ResponseEntity<Chapter> updateChapter(@Valid @RequestBody Chapter chapter) throws URISyntaxException {
        log.debug("REST request to update Chapter : {}", chapter);
        if (chapter.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Chapter result = chapterService.save(chapter);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chapter.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chapters : get all the chapters.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of chapters in body
     */
    @GetMapping("/chapters")
    public ResponseEntity<List<Chapter>> getAllChapters(Pageable pageable) {
        log.debug("REST request to get a page of Chapters");
        Page<Chapter> page = chapterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/chapters");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /chapters/:id : get the "id" chapter.
     *
     * @param id the id of the chapter to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chapter, or with status 404 (Not Found)
     */
    @GetMapping("/chapters/{id}")
    public ResponseEntity<Chapter> getChapter(@PathVariable Long id) {
        log.debug("REST request to get Chapter : {}", id);
        Optional<Chapter> chapter = chapterService.findOne(id);
        return ResponseEntity.ok(chapter.get());
    }

    /**
     * DELETE  /chapters/:id : delete the "id" chapter.
     *
     * @param id the id of the chapter to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chapters/{id}")
    public ResponseEntity<Void> deleteChapter(@PathVariable Long id) {
        log.debug("REST request to delete Chapter : {}", id);
        chapterService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/chapters?query=:query : search for the chapter corresponding
     * to the query.
     *
     * @param query the query of the chapter search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/chapters")
    public ResponseEntity<List<Chapter>> searchChapters(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Chapters for query {}", query);
        Page<Chapter> page = chapterService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/chapters");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
