package iotelesko.webProject.web.rest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import iotelesko.webProject.Services.TestsService;
import iotelesko.webProject.domains.Tests;
import iotelesko.webProject.web.rest.errors.BadRequestAlertException;
import iotelesko.webProject.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Tests.
 */
@RestController
@RequestMapping("/api")
public class TestsResource {

    private final Logger log = LoggerFactory.getLogger(TestsResource.class);

    private static final String ENTITY_NAME = "tests";

    private final TestsService testsService;

    public TestsResource(TestsService testsService) {
        this.testsService = testsService;
    }

    /**
     * POST  /tests : Create a new tests.
     *
     * @param tests the tests to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tests, or with status 400 (Bad Request) if the tests has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tests")
    public ResponseEntity<Tests> createTests(@Valid @RequestBody Tests tests) throws URISyntaxException {
        log.debug("REST request to save Tests : {}", tests);
        if (tests.getId() != null) {
            throw new BadRequestAlertException("A new tests cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tests result = testsService.save(tests);
        return ResponseEntity.created(new URI("/api/tests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tests : Updates an existing tests.
     *
     * @param tests the tests to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tests,
     * or with status 400 (Bad Request) if the tests is not valid,
     * or with status 500 (Internal Server Error) if the tests couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tests")
    public ResponseEntity<Tests> updateTests(@Valid @RequestBody Tests tests) throws URISyntaxException {
        log.debug("REST request to update Tests : {}", tests);
        if (tests.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Tests result = testsService.save(tests);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tests.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tests : get all the tests.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tests in body
     */
    @GetMapping("/tests")
    public List<Tests> getAllTests() {
        log.debug("REST request to get all Tests");
        return testsService.findAll();
    }

    /**
     * GET  /tests/:id : get the "id" tests.
     *
     * @param id the id of the tests to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tests, or with status 404 (Not Found)
     */
    @GetMapping("/tests/{id}")
    public ResponseEntity<Tests> getTests(@PathVariable Long id) {
        log.debug("REST request to get Tests : {}", id);
        Optional<Tests> tests = testsService.findOne(id);
        return ResponseEntity.ok(tests.get());
    }

    /**
     * DELETE  /tests/:id : delete the "id" tests.
     *
     * @param id the id of the tests to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tests/{id}")
    public ResponseEntity<Void> deleteTests(@PathVariable Long id) {
        log.debug("REST request to delete Tests : {}", id);
        testsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tests?query=:query : search for the tests corresponding
     * to the query.
     *
     * @param query the query of the tests search
     * @return the result of the search
     */
    @GetMapping("/_search/tests")
    public List<Tests> searchTests(@RequestParam String query) {
        log.debug("REST request to search Tests for query {}", query);
        return testsService.search(query);
    }

}
