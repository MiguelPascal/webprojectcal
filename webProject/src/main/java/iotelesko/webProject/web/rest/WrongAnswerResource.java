package iotelesko.webProject.web.rest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import iotelesko.webProject.Services.WrongAnswerService;
import iotelesko.webProject.domains.WrongAnswer;
import iotelesko.webProject.web.rest.errors.BadRequestAlertException;
import iotelesko.webProject.web.rest.util.HeaderUtil;

/**
 * REST controller for managing WrongAnswer.
 */
@RestController
@RequestMapping("/api")
public class WrongAnswerResource {

    private final Logger log = LoggerFactory.getLogger(WrongAnswerResource.class);

    private static final String ENTITY_NAME = "wrongAnswer";

    private final WrongAnswerService wrongAnswerService;

    public WrongAnswerResource(WrongAnswerService wrongAnswerService) {
        this.wrongAnswerService = wrongAnswerService;
    }

    /**
     * POST  /wrong-answers : Create a new wrongAnswer.
     *
     * @param wrongAnswer the wrongAnswer to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wrongAnswer, or with status 400 (Bad Request) if the wrongAnswer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wrong-answers")
    public ResponseEntity<WrongAnswer> createWrongAnswer(@RequestBody WrongAnswer wrongAnswer) throws URISyntaxException {
        log.debug("REST request to save WrongAnswer : {}", wrongAnswer);
        if (wrongAnswer.getId() != null) {
            throw new BadRequestAlertException("A new wrongAnswer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WrongAnswer result = wrongAnswerService.save(wrongAnswer);
        return ResponseEntity.created(new URI("/api/wrong-answers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wrong-answers : Updates an existing wrongAnswer.
     *
     * @param wrongAnswer the wrongAnswer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wrongAnswer,
     * or with status 400 (Bad Request) if the wrongAnswer is not valid,
     * or with status 500 (Internal Server Error) if the wrongAnswer couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wrong-answers")
    public ResponseEntity<WrongAnswer> updateWrongAnswer(@RequestBody WrongAnswer wrongAnswer) throws URISyntaxException {
        log.debug("REST request to update WrongAnswer : {}", wrongAnswer);
        if (wrongAnswer.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WrongAnswer result = wrongAnswerService.save(wrongAnswer);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wrongAnswer.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wrong-answers : get all the wrongAnswers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of wrongAnswers in body
     */
    @GetMapping("/wrong-answers")
    public List<WrongAnswer> getAllWrongAnswers() {
        log.debug("REST request to get all WrongAnswers");
        return wrongAnswerService.findAll();
    }

    /**
     * GET  /wrong-answers/:id : get the "id" wrongAnswer.
     *
     * @param id the id of the wrongAnswer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wrongAnswer, or with status 404 (Not Found)
     */
    @GetMapping("/wrong-answers/{id}")
    public ResponseEntity<WrongAnswer> getWrongAnswer(@PathVariable Long id) {
        log.debug("REST request to get WrongAnswer : {}", id);
        Optional<WrongAnswer> wrongAnswer = wrongAnswerService.findOne(id);
        return ResponseEntity.ok(wrongAnswer.get());
    }

    /**
     * DELETE  /wrong-answers/:id : delete the "id" wrongAnswer.
     *
     * @param id the id of the wrongAnswer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wrong-answers/{id}")
    public ResponseEntity<Void> deleteWrongAnswer(@PathVariable Long id) {
        log.debug("REST request to delete WrongAnswer : {}", id);
        wrongAnswerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wrong-answers?query=:query : search for the wrongAnswer corresponding
     * to the query.
     *
     * @param query the query of the wrongAnswer search
     * @return the result of the search
     */
    @GetMapping("/_search/wrong-answers")
    public List<WrongAnswer> searchWrongAnswers(@RequestParam String query) {
        log.debug("REST request to search WrongAnswers for query {}", query);
        return wrongAnswerService.search(query);
    }

}
