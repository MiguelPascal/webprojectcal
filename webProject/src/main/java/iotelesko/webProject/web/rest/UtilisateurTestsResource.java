package iotelesko.webProject.web.rest;

import iotelesko.webProject.domains.UtilisateurTests;
import iotelesko.webProject.Services.UtilisateurTestsService;
import iotelesko.webProject.web.rest.errors.BadRequestAlertException;

import iotelesko.webProject.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link iotelesko.webproject.domain.UtilisateurTests}.
 */
@RestController
@RequestMapping("/api")
public class UtilisateurTestsResource {

    private final Logger log = LoggerFactory.getLogger(UtilisateurTestsResource.class);

    private static final String ENTITY_NAME = "webProjectApplicationUtilisateurTests";

    private final UtilisateurTestsService utilisateurTestsService;

    public UtilisateurTestsResource(UtilisateurTestsService utilisateurTestsService) {
        this.utilisateurTestsService = utilisateurTestsService;
    }

    /**
     * {@code POST  /utilisateur-tests} : Create a new utilisateurTests.
     *
     * @param utilisateurTests the utilisateurTests to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new utilisateurTests, or with status {@code 400 (Bad Request)} if the utilisateurTests has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/utilisateur-tests")
    public ResponseEntity<UtilisateurTests> createUtilisateurTests(@RequestBody UtilisateurTests utilisateurTests) throws URISyntaxException {
        log.debug("REST request to save UtilisateurTests : {}", utilisateurTests);
        if (utilisateurTests.getId() != null) {
            throw new BadRequestAlertException("A new utilisateurTests cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UtilisateurTests result = utilisateurTestsService.save(utilisateurTests);
        return ResponseEntity.created(new URI("/api/utilisateur-tests/" + result.getId()))
        		 .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                 .body(result);
    }

    /**
     * {@code PUT  /utilisateur-tests} : Updates an existing utilisateurTests.
     *
     * @param utilisateurTests the utilisateurTests to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated utilisateurTests,
     * or with status {@code 400 (Bad Request)} if the utilisateurTests is not valid,
     * or with status {@code 500 (Internal Server Error)} if the utilisateurTests couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/utilisateur-tests")
    public ResponseEntity<UtilisateurTests> updateUtilisateurTests(@RequestBody UtilisateurTests utilisateurTests) throws URISyntaxException {
        log.debug("REST request to update UtilisateurTests : {}", utilisateurTests);
        if (utilisateurTests.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UtilisateurTests result = utilisateurTestsService.save(utilisateurTests);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, utilisateurTests.getId().toString()))
                .body(result);
    }

    /**
     * {@code GET  /utilisateur-tests} : get all the utilisateurTests.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of utilisateurTests in body.
     */
    @GetMapping("/utilisateur-tests")
    public List<UtilisateurTests> getAllUtilisateurTests() {
        log.debug("REST request to get all UtilisateurTests");
        return utilisateurTestsService.findAll();
    }

    /**
     * {@code GET  /utilisateur-tests/:id} : get the "id" utilisateurTests.
     *
     * @param id the id of the utilisateurTests to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the utilisateurTests, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/utilisateur-tests/{id}")
    public ResponseEntity<UtilisateurTests> getUtilisateurTests(@PathVariable Long id) {
        log.debug("REST request to get UtilisateurTests : {}", id);
        Optional<UtilisateurTests> utilisateurTests = utilisateurTestsService.findOne(id);
        return ResponseEntity.ok(utilisateurTests.get());
    }

    /**
     * {@code DELETE  /utilisateur-tests/:id} : delete the "id" utilisateurTests.
     *
     * @param id the id of the utilisateurTests to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/utilisateur-tests/{id}")
    public ResponseEntity<Void> deleteUtilisateurTests(@PathVariable Long id) {
        log.debug("REST request to delete UtilisateurTests : {}", id);
        utilisateurTestsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
