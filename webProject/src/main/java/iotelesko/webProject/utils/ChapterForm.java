package iotelesko.webProject.utils;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

public class ChapterForm {

	private Long chapterId;
	@NotNull
	private String chapterTitle;
	@NotNull
	private String chapterDescription;
	
	@NotNull
	private Long idModule;
	
	public ChapterForm(Long chapterId, @NotNull String chapterTitle, @NotNull String chapterDescription,
			@NotNull Long idModule) {
		super();
		this.chapterId = chapterId;
		this.chapterTitle = chapterTitle;
		this.chapterDescription = chapterDescription;
		this.idModule = idModule;
	}


	public ChapterForm() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getChapterId() {
		return chapterId;
	}


	public void setChapterId(Long chapterId) {
		this.chapterId = chapterId;
	}


	public String getChapterTitle() {
		return chapterTitle;
	}
	public void setChapterTitle(String chapterTitle) {
		this.chapterTitle = chapterTitle;
	}
	public String getChapterDescription() {
		return chapterDescription;
	}
	public void setChapterDescription(String chapterDescription) {
		this.chapterDescription = chapterDescription;
	}


	public Long getIdModule() {
		return idModule;
	}


	public void setIdModule(Long idModule) {
		this.idModule = idModule;
	}


	@Override
	public String toString() {
		return "ChapterForm [chapterId=" + chapterId + ", chapterTitle=" + chapterTitle + ", chapterDescription="
				+ chapterDescription + ", idModule=" + idModule + "]";
	}


	
	
	
}
