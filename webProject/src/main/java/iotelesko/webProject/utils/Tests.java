package iotelesko.webProject.utils;

import java.time.ZonedDateTime;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tests {
	private Long id;
    private ZonedDateTime createdDate;
    private Boolean deleted;
    private ZonedDateTime deletedDate;
    private String testTitle;
    private Long idLesson;
    
    
    
	public Tests(Long id, ZonedDateTime createdDate, Boolean deleted, ZonedDateTime deletedDate, String testTitle,
			Long idLesson) {
		super();
		this.id = id;
		this.createdDate = createdDate;
		this.deleted = deleted;
		this.deletedDate = deletedDate;
		this.testTitle = testTitle;
		this.idLesson = idLesson;
	}
	
	public Tests() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public ZonedDateTime getDeletedDate() {
		return deletedDate;
	}
	public void setDeletedDate(ZonedDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}
	public String getTestTitle() {
		return testTitle;
	}
	public void setTestTitle(String testTitle) {
		this.testTitle = testTitle;
	}
	
	
	public Long getIdLesson() {
		return idLesson;
	}
	public void setIdLesson(Long idLesson) {
		this.idLesson = idLesson;
	}
	@Override
	public String toString() {
		return "Tests [id=" + id + ", createdDate=" + createdDate + ", deleted=" + deleted + ", deletedDate="
				+ deletedDate + ", testTitle=" + testTitle + ", idLesson=" + idLesson + "]";
	}
	
}
