package iotelesko.webProject.utils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginForm {
	
	@NotNull
    @Size(min = 2, max = 30)
	private String username;
	@NotNull
	private String password;
	
	
	public LoginForm() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoginForm(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginForm [username=" + username + ", password=" + password + "]";
	}
	
	
}
