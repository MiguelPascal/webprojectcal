package iotelesko.webProject.utils;

import javax.validation.constraints.NotNull;

public class ModuleForm {
	

	private Long moduleId;
	@NotNull
	private String moduleTitle;
	@NotNull
	private String moduleDescription;
	
	
	
	public ModuleForm() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getModuleId() {
		return moduleId;
	}
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleTitle() {
		return moduleTitle;
	}
	public void setModuleTitle(String moduleTitle) {
		this.moduleTitle = moduleTitle;
	}
	public String getModuleDescription() {
		return moduleDescription;
	}
	public void setModuleDescription(String moduleDescription) {
		this.moduleDescription = moduleDescription;
	}
	@Override
	public String toString() {
		return "ModuleForm [moduleId=" + moduleId + ", moduleTitle=" + moduleTitle + ", moduleDescription="
				+ moduleDescription + "]";
	}
	
	

}
