package iotelesko.webProject.utils;

import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Utilisateur {
	private long id;
	private String username;
	private String password;
	private String phone;
	private ZonedDateTime createdDate;
	private boolean deleted;
	private ZonedDateTime deletedDate;
	private String role;
	private List<Tests> tests;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public ZonedDateTime getDeletedDate() {
		return deletedDate;
	}
	public void setDeletedDate(ZonedDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public List<Tests> getTests() {
		return tests;
	}
	public void setTests(List<Tests> tests) {
		this.tests = tests;
	}
	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", username=" + username + ", password=" + password + ", phone=" + phone
				+ ", createdDate=" + createdDate + ", deleted=" + deleted + ", deletedDate=" + deletedDate + ", role="
				+ role + ", tests=" + tests + "]";
	}
	
}
