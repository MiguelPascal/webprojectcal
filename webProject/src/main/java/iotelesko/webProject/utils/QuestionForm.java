package iotelesko.webProject.utils;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import iotelesko.webProject.domains.Tests;
import iotelesko.webProject.domains.enumeration.QuestionType;

public class QuestionForm {

	
    private Long questionId;

    private ZonedDateTime createdDate;

    private String rightAnswer;

    private String questionTitle;

    private Integer questionScore;

    private String questionType;

    private Long idTest;
    
    @NotNull
    private String wrongAnswer1;
    
    private String wrongAnswer2;
    private String wrongAnswer3;
    
	

	public QuestionForm() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public String getRightAnswer() {
		return rightAnswer;
	}

	public void setRightAnswer(String rightAnswer) {
		this.rightAnswer = rightAnswer;
	}

	public String getQuestionTitle() {
		return questionTitle;
	}

	public void setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
	}

	public Integer getQuestionScore() {
		return questionScore;
	}

	public void setQuestionScore(Integer questionScore) {
		this.questionScore = questionScore;
	}
	
	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public Long getIdTest() {
		return idTest;
	}

	public void setIdTest(Long idTest) {
		this.idTest = idTest;
	}

	public String getWrongAnswer1() {
		return wrongAnswer1;
	}

	public void setWrongAnswer1(String wrongAnswer1) {
		this.wrongAnswer1 = wrongAnswer1;
	}

	public String getWrongAnswer2() {
		return wrongAnswer2;
	}

	public void setWrongAnswer2(String wrongAnswer2) {
		this.wrongAnswer2 = wrongAnswer2;
	}

	public String getWrongAnswer3() {
		return wrongAnswer3;
	}

	public void setWrongAnswer3(String wrongAnswer3) {
		this.wrongAnswer3 = wrongAnswer3;
	}

	@Override
	public String toString() {
		return "QuestionForm [questionId=" + questionId + ", createdDate=" + createdDate + ", rightAnswer="
				+ rightAnswer + ", questionTitle=" + questionTitle + ", questionScore=" + questionScore
				+ ", questionType=" + questionType + ", idTest=" + idTest + ", wrongAnswer1=" + wrongAnswer1
				+ ", wrongAnswer2=" + wrongAnswer2 + ", wrongAnswer3=" + wrongAnswer3 + "]";
	}
    
}
