/*
 * package iotelesko.webProject.Configuration;
 * 
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.security.config.annotation.authentication.builders.
 * AuthenticationManagerBuilder; import
 * org.springframework.security.config.annotation.web.builders.HttpSecurity;
 * import org.springframework.security.config.annotation.web.configuration.
 * EnableWebSecurity; import
 * org.springframework.security.config.annotation.web.configuration.
 * WebSecurityConfigurerAdapter;
 * 
 * @EnableWebSecurity public class SecurityConfig extends
 * WebSecurityConfigurerAdapter {
 * 
 * @Override protected void configure(HttpSecurity http) throws Exception { http
 * .authorizeRequests() .antMatchers("/resources/**",
 * "/login","/database/**","/register").permitAll()
 * .antMatchers("/student/**").hasRole("USER")
 * .antMatchers("/admin/**").hasRole("ADMIN") .anyRequest().authenticated()
 * .and() .formLogin() .loginPage("/").failureUrl("/login-error") .permitAll();
 * }
 * 
 * @Autowired public void configureGlobal(AuthenticationManagerBuilder auth)
 * throws Exception { auth .inMemoryAuthentication()
 * .withUser("user").password("password").roles("USER"); } }
 */