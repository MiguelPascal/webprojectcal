 insert into Utilisateur(id,username,jhi_role,jhi_password,phone) values (1,'miguel','ADMIN','miguel',670548172);

insert into Utilisateur(id,username,jhi_role,jhi_password,phone) values (2,'miguelito','STUDENT','miguel',694981023);

insert into Utilisateur(id,username,jhi_role,jhi_password,phone) values (3,'ismaelle','STUDENT','ismaelle',694823341);

insert into Utilisateur values (4,curdate(),false,null,'Paul','TAMO','paul',68589825,'STUDENT','M','paul');
insert into Utilisateur values (5,curdate(),false,null,'charles','TENE','charles',68545765,'STUDENT','M','charles');
insert into Utilisateur values (6,curdate(),false,null,'Peter','Roger','peter',67965343,'STUDENT','M','roger');
insert into Utilisateur values (7,curdate(),false,null,'Pressure','WINY','pressure',698978545,'STUDENT','F','pressure');
insert into Utilisateur values (8,curdate(),false,null,'Benny','IGWO','benny',67123456,'STUDENT','F','benny');






insert into Module(id, created_date, deleted, deleted_date, description, module_title) values (1,CURRENT_DATE(),false,null,'This first module seeks to develop in the learner basic competencies needed in the use of computers and computing related tools.','THE COMPUTING ENVIRONMENT');

insert into Module(id, created_date, deleted, deleted_date, description, module_title) values (2,CURRENT_DATE(),false,null,'This second module presents a collection of competencies which would cause the learner to progressively learn the first concepts in computer architecture and software.','COMPUTER HARDWARE, SOFTWARE, AND BASIC CONCEPTS');





insert into Chapter(id, chapter_description, chapter_title, created_date, deleted, deleted_date, module_id) values (001,'identify some areas of computer applications; enumerate types of software; enumerate examples of system software, applications software and select material and software needs for specific purpose','selecting and determining Hardware and Software needs',CURRENT_DATE(),false,null,1);

insert into Chapter(id, chapter_description, chapter_title, created_date, deleted, deleted_date, module_id) values (002,'Reproduce the basic functional diagram of a computer and open, close and exit your computer programs','manipulation of the computer',CURRENT_DATE(),false,null,1);

insert into Chapter(id, chapter_description, chapter_title, created_date, deleted, deleted_date, module_id) values (003,'Writting basic solution procedure to problems and ordering solution steps','Initiation to algorithmic thinking',CURRENT_DATE(),false,null,1);



insert into Chapter values (004,'Discover and use appropriate Input and Output peripherals','Input/Output Peripherals',CURDATE(),false,null,1);
insert into Chapter values (005,'Discovering and navigating the Internet ','Discovery of the Internet',CURDATE(),false,null,1);

insert into Chapter values (006,'Discover the functionality of a computer and the use of basic software and Manage and conserve data on storage devices or facilities','Recognition of Input and Output peripherals',CURDATE(),false,null,2);
insert into Chapter values (007,'Carry out basic computer maintenance and the Use the computer to carry out basic tasks','Basic computer maintenance',CURDATE(),false,null,2);



insert into Lesson(id, created_date, deleted, deleted_date, lesson_content, lesson_title, pdf_content,pdf_content_content_type, chapter_id) values (1,CURRENT_DATE(),false,'2020-06-08 00:00:00','by audio,video,pdf','IDENTIFICATION OF SOME AREAS OF COMPUTER APPLICATIONS',null,'pdf',001);

insert into Lesson(id, created_date, deleted, deleted_date, lesson_content, lesson_title, pdf_content,pdf_content_content_type, chapter_id) values (2,CURRENT_DATE(),false,'2020-06-08 00:00:00','by audio,video,pdf','TYPE OF SOFTWARE',null,null,001);

insert into Lesson(id, created_date, deleted, deleted_date, lesson_content, lesson_title, pdf_content,pdf_content_content_type, chapter_id) values (3,CURRENT_DATE(),false,'2020-06-08 00:00:00','by audio,video,pdf','EXAMPLES OF SYSTEM SOFTWARE',null,null,001);

insert into Lesson(id, created_date, deleted, deleted_date, lesson_content, lesson_title, pdf_content,pdf_content_content_type, chapter_id) values (4,CURRENT_DATE(),false,'2020-06-08 00:00:00','by audio,video,pdf','EXAMPLES OF APPLICATIONS SOFTWARE',null,null,001);

insert into Lesson(id, created_date, deleted, deleted_date, lesson_content, lesson_title, pdf_content,pdf_content_content_type, chapter_id) values (5,CURRENT_DATE(),false,'2020-06-08 00:00:00','by audio,video,pdf','SELECTION OF MATERIALS AND SOFTWARE NEEDS',null,null,001);

insert into Lesson(id, created_date, deleted, deleted_date, lesson_content, lesson_title, pdf_content, pdf_content_content_type,chapter_id) values (6,CURRENT_DATE(),false,'2020-06-08 00:00:00','by audio,video,pdf','BASIC FUNCTIONAL DIAGRAM OF COMPUTER',null,null,002);

insert into Lesson(id, created_date, deleted, deleted_date, lesson_content, lesson_title, pdf_content, pdf_content_content_type,chapter_id) values (7,CURRENT_DATE(),false,'2020-06-08 00:00:00','by audio,video,pdf','INPUT AND OUTPUT PERIPHERALS',null,null,002);

insert into Lesson(id, created_date, deleted, deleted_date, lesson_content, lesson_title, pdf_content, pdf_content_content_type,chapter_id) values (8,CURRENT_DATE(),false,'2020-06-08 00:00:00','by audio,video,pdf','PRINCIPAL PART OF CENTRAL PROCESSING UNIT',null,null,002);




insert into Tests(id, created_date, deleted, deleted_date, test_title, lesson_id) values (1,CURRENT_DATE(),false,null,'KNOWLEDGE TEST 1',1);

insert into Tests(id, created_date, deleted, deleted_date, test_title, lesson_id) values (2,CURRENT_DATE(),false,null,'KNOWLEDGE TEST 1',2);

insert into Tests(id, created_date, deleted, deleted_date, test_title, lesson_id) values (3,CURRENT_DATE(),false,null,'KNOWLEDGE TEST 2',1);

insert into Tests(id, created_date, deleted, deleted_date, test_title, lesson_id) values (4,CURRENT_DATE(),false,null,'KNOWLEDGE TEST 2',2);




insert into Question(id, created_date, deleted, deleted_date, question_score,question_title, question_type, right_answer, tests_id) values (1,CURRENT_DATE(),false,null,1,'The following words are not the areas of computer application','MCQ','E:none of the above',1);



insert into Question(id, created_date, deleted, deleted_date, question_score,question_title, question_type, right_answer, tests_id) values (2,curdate(),false,null,1,'The following words are type of software','MCQ','A:Microsoft Word; B: Mozilla firefox',1);
insert into Question values (3,curdate(),false,null,1,'what is the name of the image below?','MCQ','A: A computer',1);
insert into Question values (4,curdate(),false,null,1,'which part of computer allows to display information?','MCQ','A: A Monitor',1);
insert into Question values (5,curdate(),false,null,1,'which part of computer allows to enter information in the computer?','MCQ','B: A Keyboard',1);





insert into Wrong_answer(id, created_date, deleted, deleted_date, wrong_answer, question_id) values (1,CURRENT_DATE(),false,null,'A: Medical domain',1);

insert into Wrong_answer(id, created_date, deleted, deleted_date, wrong_answer, question_id) values (2,CURRENT_DATE(),false,null,'B: electronic domain',1);

insert into Wrong_answer(id, created_date, deleted, deleted_date, wrong_answer, question_id) values (3,CURRENT_DATE(),false,null,'C: military domain',1);


insert into Wrong_answer(id, created_date, deleted, deleted_date, wrong_answer, question_id) values (4,CURRENT_DATE(),false,null,'D: Medical domain',1);

insert into Wrong_answer(id, created_date, deleted, deleted_date, wrong_answer, question_id) values (5,curdate(),false,null,'C: Disque Dur',2);
insert into Wrong_answer(id, created_date, deleted, deleted_date, wrong_answer, question_id) values (6,curdate(),false,null,'D: Memoire',2);

insert into Wrong_answer values (7,curdate(),false,null,'B: A Television',3);
insert into Wrong_answer values (8,curdate(),false,null,'C: A radio',3);
insert into Wrong_answer values (9,curdate(),false,null,'D: A printer',3);

insert into Wrong_answer values (10,curdate(),false,null,'B: A Television',4);
insert into Wrong_answer values (11,curdate(),false,null,'C: A computer',4);
insert into Wrong_answer values (12,curdate(),false,null,'D: A printer',4);

insert into Wrong_answer values (13,curdate(),false,null,'A: A printer',5);
insert into Wrong_answer values (14,curdate(),false,null,'C: A Mouse',5);
insert into Wrong_answer values (15,curdate(),false,null,'D: A Monitor',5);